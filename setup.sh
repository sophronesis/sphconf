#!/usr/bin/env bash

RICEPATH=$(dirname $(realpath $0))
CONFIG=$HOME/.config/

# Optional copy to .bak file before copying ricing files (on demand)
function _cpsafe {
  #if [ -e $2 ]; then
    #rm -r $2.bak
    #cp -r $2 $2.bak
  #fi
  cp -r $1 $2
}

function _cponce {
  if [ ! -e $2 ]; then
    cp -r $1 $2
  fi
}

# make local editable copy of sensitive data like tokens and passwords
mkdit "$CONFIG" 2>/dev/null
mkdir "$CONFIG/sphrice" 2>/dev/null
chmod 700 "$CONFIG/sphrice"
_cponce "$RICEPATH/configs/tgsh.conf.def" "$RICEPATH/configs/tgsh.conf"
_cpsafe "$RICEPATH/configs/tgsh.conf" "$CONFIG/sphrice/tgsh.conf"
_cponce "$RICEPATH/configs/poly_ttrss.conf.def" "$RICEPATH/configs/poly_ttrss.conf"
_cpsafe "$RICEPATH/configs/poly_ttrss.conf" "$CONFIG/sphrice/poly_ttrss.conf"
_cponce "$RICEPATH/configs/systemDep.conf.def" "$RICEPATH/configs/systemDep.conf"
_cpsafe "$RICEPATH/configs/systemDep.conf" "$CONFIG/sphrice/systemDep.conf"

# copy all configs to correct places
_cpsafe "$RICEPATH/configs/.zshrc" "$HOME/.zshrc"
mkdir "$CONFIG/nvim/" 2>/dev/null
mkdir "$CONFIG/sphutils/" 2>/dev/null
mkdir "$CONFIG/wildpipes/" 2>/dev/null
_cpsafe "$RICEPATH/configs/init.vim" "$CONFIG/nvim/init.vim"
_cpsafe "$RICEPATH/sphutils/*" "$CONFIG/sphutils"
_cpsafe "$RICEPATH/wildpipes/*" "$CONFIG/wildpipes"
_cpsafe "$RICEPATH/awesome" "$CONFIG"
_cpsafe "$RICEPATH/polybar" "$CONFIG"
_cpsafe "$RICEPATH/polybar-modules" "$CONFIG"

# add lang file
_cponce "$RICEPATH/configs/.lang" "$HOME/.lang"


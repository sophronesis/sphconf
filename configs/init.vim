call plug#begin("$HOME/.config/nvim/plugs")
  " https://vimawesome.com/plugin/nerdtree-red
  Plug 'preservim/nerdtree'
  " https://vimawesome.com/plugin/vim-airline-superman
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'valloric/youcompleteme'
call plug#end()
let g:ycm_global_ycm_extra_conf = '$HOME/config/nvim/ycm_extra_conf.py'
set number
set relativenumber
set autoindent
set lazyredraw
set tabstop=2
set expandtab!
set shiftwidth=2
syntax on 
" from https://www.youtube.com/watch?v=E_rbfQqrm7g 
"vnoremap <C-c> \"+y
" vnoremap <C-c> "*y :let @+=@*<CR>
"map <C-v> "+p
"folding settings
set foldmethod=syntax "indent   "fold based on indent
set foldnestmax=10      "deepest fold is 10 levels
set nofoldenable        "dont fold by default
set foldlevel=1         "this is just what i use
iab xdate <c-r>=strftime("[%Y/%m/%d-%a-%H:%M:%S]>")<cr>

" send t buffer to telegram
command Tgclip execute ":call system(\"tgclip\", getreg(\"t\"))"



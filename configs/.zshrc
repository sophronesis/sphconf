# +Main
export ZSH="$HOME/.oh-my-zsh"
export LANG="C.UTF-8"
export EDITOR='nvim'
export TERMINAL='st'

ZSH_THEME="clean"
plugins=(git)
source $ZSH/oh-my-zsh.sh
source $HOME/.config/sphrice/tgsh.conf
source $HOME/.config/sphrice/poly_ttrss.conf
source $HOME/.config/sphrice/systemDep.conf
source $HOME/.config/wildpipes/wp_aliases
# -Main

# +Aliases
function _cmdUsable() {
  local FPATH=$(which $1 2>/dev/null)
  if [[ -f "$FPATH" ]]; then
    echo "1"
  else
    echo "0"
  fi
}

if [[ $(_cmdUsable cpg) -eq 1 ]]; then
  #echo "cpg found"
  alias cp='cpg -g'
  alias mv='mvg -g'
fi
alias ll='ls -lah'
alias ..='cd ..'
alias reboot='sudo /sbin/reboot'
alias poweroff='sudo /sbin/poweroff'
alias c='clear'

alias iclip="xclip -selection c" # *clip executables/aliases are for clipboard manipulation
alias oclip="xclip -selection c -o"
alias tbclip="nc termbin.com 9999"
alias tbiclip="tbclip | iclip"
alias screenshot='maim -s -u | xclip -selection clipboard -t image/png -i'
alias wtch="watch -n0,1"

if [[ $(_cmdUsable nvim) -eq 1 ]]; then
  #echo "nvim found"
  alias vim="nvim"
else
  if [[ $(_cmdUsable vi) -eq 1 ]]; then
    #echo "vi found"
    alias vim="vi"
  fi
fi 
if [[ $(_cmdUsable bat) -eq 1 ]]; then
  #echo "bat found"
  alias cat="bat"
fi
alias ff="fzf --preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'"
alias beep="play -q -n synth 0.1 sin 880"
# allows ranger to jump to last visited directory in given terminal
# more here https://github.com/ranger/ranger/wiki/Integration-with-other-programs#changing-directories
alias ranger=". ranger"
# alias mpd_mount="sudo mount /dev/sda2 /mnt && mpd && mpc update"
alias getip="curl ifconfig.me"
alias xdate="date \"+[%Y/%m/%d-%a-%H:%M:%S]>\""
alias vfzf="vim \$(fzf)"
# -Aliases

# +Prompt
function preexec() {
  timer=$(date +%s%3N)
}

function precmd() {
  if [ $timer ]; then
    local now=$(date +%s%3N)
    local d_ms=$(($now-$timer))
    local d_s=$((d_ms / 1000))
    local ms=$((d_ms % 1000))
    local s=$((d_s % 60))
    local m=$(((d_s / 60) % 60))
    local h=$((d_s / 3600))
    if ((h > 0)); then elapsed=${h}h${m}m
    elif ((m > 0)); then elapsed=${m}m${s}s
    elif ((s >= 10)); then elapsed=${s}.$((ms / 100))s
    elif ((s > 0)); then elapsed=${s}.$((ms / 10))s
    else elapsed=${ms}ms
    fi

    export RPROMPT="%F{cyan}took ${elapsed} %{$reset_color%} ⟪%*⟫"
    unset timer
  else
    export RPROMPT='⟪%*⟫'
  fi
}

PROMPT='%{$fg[$NCOLOR]%}%B%n%b%{$reset_color%}($SYSTEM_ZSH_LETTER):%{$fg[blue]%}%B%c/%b%{$reset_color%} $(git_prompt_info)%(!.#.∮) '
RPROMPT='⟪%*⟫'
# -Prompt

# +Path extension
export PATH="$HOME/.config/polybar-modules:$PATH"
export PATH="$HOME/.config/sphutils:$PATH"
export PATH="$HOME/.config/wildpipes:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/Telegram:$PATH"
# -Path extension

# Lines configured by zsh-newuser-install
unsetopt autocd
# End of lines configured by zsh-newuser-install
